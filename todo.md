- Grapher: show node and relation info on rollover and click
    
    clickable through 
    https://magento2x.com/htmlcss-make-clickable-transparent-div-layer/
    
    draggable window
    https://www.w3schools.com/howto/howto_js_draggable.asp

- Graph Options: nodeAutoColorBy, nodeLabel


- other types of visualization.

- replace imp exp by icons.


- (postpone) Recordviewer: css grid 

- move removeNeo4jTypes() sanitize() serialize() from Recordviewer.svelte to formatting.js