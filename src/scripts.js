export let scripts ={
    'first': 
    {
        cypher:`MATCH (b:Broadcast {id:354})--(q) 
RETURN b.id as source ,q.id AS target 
LIMIT 100;                
`,
        javascript:`             
const links = result.detail.records.map(r => { 
return {source:r.get('source').toNumber(), target:r.get('target').toNumber()}
});        
const ids = new Set()
links.forEach(l => {ids.add(l.source);ids.add(l.target);});
const gData = { nodes: Array.from(ids).map(id => {return {id:id}}), links: links}
return gData
`,
    }
}

// //debugger

// const links = result.detail.records.map(r => {
//     //debugger  
//     return {
//         source: r.get('source').toNumber(),
//         target: r.get('target').toNumber(),
//     }
// });        

// const ids = new Set()
// links.forEach(l => {
//   ids.add(l.source);
//   ids.add(l.target);
// });

// const gData = { 
//   nodes: Array.from(ids).map(idd => {id:idd}), 
//   links: links
// };
// return gData

